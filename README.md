# BikeMoves Illinois Mapbox GL Style
The map style used in the [BikeMoves Illinois](http://bikemoves.me/)
mobile app. Based on Mapbox's
[bright-v9](https://github.com/mapbox/mapbox-gl-styles).
